##Welcome

This source code repository houses data analysis source code 
and processed data files from a heat stress RNA-Seq experiment 
on tomato pollen that was performed in the laboratory of Nurit
Furon. Sequencing was sponsored by the 
Pollen Research Coordination Network and will be used to introduce
RNA-Seq data analysis methods at the
[2014 UNC Charlotte Workshop in Next-Generation Sequencing](http://wings.iplantcollaborative.org).

Results obtained during the workshop will then be discussed that evening
and the following day at the 
the [2014 meeting of the Pollen Research Coordination Network](http://pollennetwork.org/?q=content/pollen-ngs-workshop-and-4th-annual-pollen-rcn-meeting-may-13-15-2014).

**DISCLAIMER**: These data are not yet published and are subject to
change. Prior to publication, we reserve the right to change any file
and also draw new conclusions from the data as the analysis progresses.

##The samples

Tomato plants of cultivar Hazera 3042, a heat-tolerant variety, were grown under
control and mild chronic heat shock conditions as described in 
[Frank, et al (2009)](http://www.ncbi.nlm.nih.gov/pmc/articles/PMC2736902/).
Five times
during the summer and fall of 2013, pollen samples were collected from the plants.
RNA was extracted and used to make cDNA libaries for Illumina sequencing.

##Data processing and differential expression analysis

Sequence reads were aligned onto version 2.5 of the Solanum
lycopersicum genome assembly using the tophat spliced alignment
tool. Alignments of cDNA fragments overlapping annotated
ITAG2.4 genes were counted using featureCounts. Differential expression of tomato
genes under the heat stress was detected using edgeR. 

##Visualization of read alignments

Alignments of reads and junction features deduced from splice read alignments
can be viewed using Integrated Genome Browser, which is freely available from
[BioViz.org](http://bioviz.org/igb). 

To view the data in IGB:

* Get a copy of IGB (http://www.bioviz.org)
* Under the **Current Genome** tab, select species S. lycopersicum and the Feb. 2014 (2.5) genome version.
* The tomato pollen data are available in the **Data Access** tab in the folder named **Pollen**
